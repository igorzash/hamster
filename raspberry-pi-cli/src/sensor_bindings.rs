#[link(name = "mh_z19b")]
extern "C" {
    fn mh_z19b_read_co2_concentration_from_default_device() -> i32;
}

#[link(name = "sht21")]
extern "C" {
    fn sht21_get_temperature() -> f64;
    fn sht21_get_humidity() -> f64;
}

#[link(name = "max44009")]
extern "C" {
    fn max44009_get_luminance() -> f64;
}

#[link(name = "ads1115")]
extern "C" {
    fn ads1115_get_voltage(channel: u32) -> i32;
}

#[repr(C)]
pub struct ccs811_output {
    pub error: u32,
    pub eco2: u32,
    pub tvoc: u32
}

#[link(name = "ccs811")]
extern "C" {
    fn ccs811_start() -> i32;
    fn ccs811_get() -> ccs811_output;
    fn ccs811_set_mode(mode: u32) -> i32;
}

#[repr(C)]
pub struct pms1003_output {
    pub error: u32,
    pub particles: [u32; 12]
}

#[link(name = "pms1003")]
extern "C" {
    fn pms1003_get_particles() -> pms1003_output;
}

pub fn get_co2() -> Option<i32> {
    let co2: i32;

    unsafe {
        co2 = mh_z19b_read_co2_concentration_from_default_device();
    }

    if co2 < 0 {
        return None
    }

    Some(co2)
}

pub fn get_temperature() -> Option<f64> {
    let temp: f64;

    unsafe {
        temp = sht21_get_temperature();
    }

    if temp < 0.0 {
        return None;
    }

    Some(temp)
}

pub fn get_humidity() -> Option<f64> {
    let humidity: f64;

    unsafe {
        humidity = sht21_get_humidity();
    }

    if humidity < 0.0 {
        return None;
    }

    Some(humidity)
}

pub fn get_luminance() -> Option<f64> {
    let luminance: f64;

    unsafe {
        luminance = max44009_get_luminance();
    }

    if luminance < 0.0 {
        return None;
    }

    Some(luminance)
}

pub fn get_gas_concentration() -> Option<i32> {
    let gas_concentration: i32;

    unsafe {
        gas_concentration = ads1115_get_voltage(0);
    }

    if gas_concentration < 0 {
        return None;
    }

    Some(gas_concentration)
}

pub fn get_dust_level() -> Option<i32> {
    let dust_level: i32;

    unsafe {
        dust_level = ads1115_get_voltage(1);
    }

    if dust_level < 0 {
        return None;
    }

    Some(dust_level)
}

pub fn start_ccs811() {
    unsafe {
        ccs811_start();
    }
}

pub fn set_ccs811_mode(mode: u32) {
    unsafe {
        ccs811_set_mode(mode);
    }
}

pub fn get_eco2_tvoc() -> ccs811_output {
    unsafe {
        return ccs811_get();
    }
}

pub fn get_particles() -> pms1003_output {
    unsafe {
        let mut particles = pms1003_get_particles();

        while particles.error != 0 {
            particles = pms1003_get_particles();
        }

        return particles;
    }
}
