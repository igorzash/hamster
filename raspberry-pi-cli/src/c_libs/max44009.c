#include "max44009.h"

double max44009_get_luminance()
{
  int file, msb, lsb, exponent, mantissa;
  unsigned char data[2];
  double luminance;

  if ((file = open("/dev/i2c-1", O_RDWR)) < 0)
  {
    return -1;
  }
  // Get I2C device
  ioctl(file, I2C_SLAVE, 74);

  // Select configuration register(0x02)
  // Continuous mode, Integration time = 800 ms(0x00)
  data[0] = 0x02;
  data[1] = 0x00;
  write(file, data, 2);
  sleep(1);

  // Read data from register(0x03)
  // luminance MSB
  data[0] = 0x03;
  write(file, data, 1);
  if (read(file, data, 1) != 1)
  {
    return -1;
  }
  msb = data[0];
  // Read data from register(0x03)
  // luminance MSB
  data[0] = 0x04;
  write(file, data, 1);
  if (read(file, data, 1) != 1)
  {
    return -1;
  }
  lsb = data[0];
  // printf("data: %02X %02X", msb, lsb);
  // Convert the data to lux
  exponent = (msb & 0xF0) >> 4;
  mantissa = ((msb & 0x0F) << 4) | (lsb & 0x0F);
  luminance = pow(2, exponent) * mantissa * 0.045;

  return luminance;
}
