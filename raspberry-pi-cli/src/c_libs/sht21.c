#include "sht21.h"

unsigned int sht21_command(int fd, unsigned char command)
{
  unsigned char buffer[2];

  if (write(fd, &command, 1) != 1) {
    close(fd);
    perror("sht21 write error");
    exit(-1);
  }

  usleep(100000); // sleep 100 msec
  if (read(fd, buffer, 2) != 2) {
    close(fd);
    perror("sht21 read error");
    exit(-1);
  }

  return ((unsigned int)buffer[0] << 8) | buffer[1];
}

int sht21_get_fd() {
  int fd;

  // open device on /dev/i2c-1 the default on Raspberry Pi B
  if ((fd = open("/dev/i2c-1", O_RDWR)) < 0) {
    return -1;
  }

  // connect to sht21 as i2c slave
  if (ioctl(fd, I2C_SLAVE, SHT21_ADDRESS) < 0) {
      close(fd);
      return -1;
  }

  return fd;
}

double sht21_get_humidity()
{
  int fd = sht21_get_fd();

  double humidity = (-6.0 + 125.0 / 65536.0 * (double)(sht21_command(fd, SHT21_TRIGGER_HUMD_MEASURE_NOHOLD)));

  close(fd);

  return humidity;
}

double sht21_get_temperature()
{
  int fd = sht21_get_fd();

  double temp = (-46.85 + 175.72 / 65536.0 * (double)(sht21_command(fd, SHT21_TRIGGER_TEMP_MEASURE_NOHOLD)));

  close(fd);

  return temp;
}
