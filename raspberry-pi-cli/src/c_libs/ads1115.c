#include "ads1115.h"


int ads1115_get_voltage(unsigned int channel)
{
  int fd;
  unsigned char buffer[3];
  unsigned int val, uv;

  // open device on /dev/i2c-1
  if ((fd = open("/dev/i2c-1", O_RDWR)) < 0)
  {
    return -1;
  }

  // connect to ads1115 as i2c slave
  if (ioctl(fd, I2C_SLAVE, ADS1115_ADDRESS) < 0)
  {
    close(fd);
    return -1;
  }
  
  // set config register and start conversion
  // single-ended, 2.048v, 8SPS
  buffer[0] = 1;    // config register is 1
  buffer[1] = 0b11000101 | (channel << 4);
  buffer[2] = 0b00000011;
  if (write(fd, buffer, 3) != 3) {
    close(fd);
    return -1;
  }
  
  // wait for conversion complete
  do {
    usleep(100000); // sleep 100 msec
    
    if (read(fd, buffer, 2) != 2)
    {
      close(fd);
      return -1;
    }

  } while ((buffer[0] & 0x80) == 0);
  
  // read conversion register
  buffer[0] = 0;   // conversion register is 0
  if (write(fd, buffer, 1) != 1) {
    close(fd);
    return -1;
  }
  if (read(fd, buffer, 2) != 2) {
    close(fd);
    return -1;
  }
  
  // convert output and display results
  val = buffer[0];
  val = (val << 8) + buffer[1];
  uv = val * (2048000 / 32768);
  close(fd);

  return uv;
}
