#ifndef CCS811_H
#define CCS811_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <inttypes.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <string.h>
#include <time.h>

#define CCS811_APP_START_COMMAND 0xF4
#define CCS811_MEAS_MODE_REGISTER 1
#define CCS811_ALG_RESULT_DATA_REGISTER 2
#define CCS811_ADDRESS 0x5B
#define CCS811_STATUS_REGISTER 0

struct ccs811_output
{
    unsigned int error;
    unsigned int eco2;
    unsigned int tvoc;
};

#endif // CCS811_H
