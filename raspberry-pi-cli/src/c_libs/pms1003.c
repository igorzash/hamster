#include "pms1003.h"

int pms1003_set_interface_attribs(int fd, int speed, int parity)
{
  struct termios tty;

  memset(&tty, 0, sizeof tty);
  if (tcgetattr(fd, &tty))
  {
    printf("error %d from tcgetattr\n", errno);
    return 1;
  }

  cfsetospeed(&tty, speed);
  cfsetispeed(&tty, speed);

  tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
  // disable IGNBRK for mismatched speed tests; otherwise receive break
  // as \000 chars
  tty.c_iflag &= ~IGNBRK;         // disable break processing
  tty.c_lflag = 0;                // no signaling chars, no echo,
  // no canonical processing
  tty.c_oflag = 0;                // no remapping, no delays
  tty.c_cc[VMIN] = 0;            // read doesn't block
  tty.c_cc[VTIME] = 5;            // 0.5 second read timeout

  tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

  tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
  // enable reading
  tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
  tty.c_cflag |= parity;
  tty.c_cflag &= ~CSTOPB;
  tty.c_cflag &= ~CRTSCTS;

  if (tcsetattr(fd, TCSANOW, &tty))
  {
    printf("error %d from tcsetattr\n", errno);
    return 1;
  }
  return 0;
}

struct pms1003_output pms1003_get_particles()
{
  struct pms1003_output output;
  output.error = 1;

  unsigned char buffer[256];
  int n, i;

  int fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_SYNC);
  if (fd < 0)
  {
    return output;
  }

  if (pms1003_set_interface_attribs(fd, B9600, 0))
  {
    close(fd);
    return output;
  }

  i = 0;

  do
  {
    n = read(fd, &buffer[i], 1);
  } while (!n);

  while (n)
  {
    i++;
    n = read(fd, &buffer[i], 1);
  }

  if (buffer[0] != 0x42 || buffer[1] != 0x4D || buffer[2] != 0 || buffer[3] != 0x1C)
  {
    close(fd);
    return output;
  }

  for (i = 1; i <= 12; i++)
  {
    unsigned int value = (buffer[i*2+2] << 8) | buffer[i*2+3];

    output.particles[i-1] = value;
  }

  close(fd);

  output.error = 0;
  return output;
}
