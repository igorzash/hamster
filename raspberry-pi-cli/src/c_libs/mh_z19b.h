#ifndef MH_Z19B_H
#define MH_Z19B_H

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

int mh_z19b_read_co2_concentration(char* device);

int mh_z19b_read_co2_concentration_from_default_device();

#endif // MH_Z19B_H