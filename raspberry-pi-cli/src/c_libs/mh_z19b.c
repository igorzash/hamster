#include "mh_z19b.h"

unsigned char READ_CO2[] = { 0xFF, 0x01, 0x86, 0, 0, 0, 0, 0, 0x79 };

int mh_z19b_set_interface_attribs(int fd, int speed, int parity)
{
  struct termios tty;

  memset(&tty, 0, sizeof tty);
  if (tcgetattr(fd, &tty))
  {
    printf("error %d from tcgetattr\n", errno);
    return -1;
  }

  cfsetospeed(&tty, speed);
  cfsetispeed(&tty, speed);

  tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
  // disable IGNBRK for mismatched speed tests; otherwise receive break
  // as \000 chars
  tty.c_iflag &= ~IGNBRK;         // disable break processing
  tty.c_lflag = 0;                // no signaling chars, no echo,
  // no canonical processing
  tty.c_oflag = 0;                // no remapping, no delays
  tty.c_cc[VMIN] = 0;            // read doesn't block
  tty.c_cc[VTIME] = 50;            // 5 seconds read timeout

  tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

  tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
  // enable reading
  tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
  tty.c_cflag |= parity;
  tty.c_cflag &= ~CSTOPB;
  tty.c_cflag &= ~CRTSCTS;

  if (tcsetattr(fd, TCSANOW, &tty))
  {
    printf("error %d from tcsetattr\n", errno);
    return -1;
  }
  return 0;
}

int mh_z19b_read_co2_concentration(char* device)
{
  unsigned char buffer[9];
  int n, i;

  int fd = open(device, O_RDWR | O_NOCTTY | O_SYNC);
  if (fd < 0)
  {
    printf("error %d opening %s: %s\n", errno, device, strerror(errno));
    return 1;
  }

  if (mh_z19b_set_interface_attribs(fd, B9600,
                            0))  // set speed to 9600 bps, 8n1 (no parity)
  {
    close(fd);
    return 1;
  }

  if (write(fd, READ_CO2, 9) != 9)
  {
    printf("write error\n");
    close(fd);
    return 1;
  }

  for (i = 0; i < 9; i++)
  {
    n = read(fd, &buffer[i], 1);
    if (!n)
      break;
  }

  close(fd);
  
  return ((unsigned int)buffer[2] << 8) | buffer[3];
}

int mh_z19b_read_co2_concentration_from_default_device() {
  return mh_z19b_read_co2_concentration("/dev/serial0");
}