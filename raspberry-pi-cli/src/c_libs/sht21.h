#ifndef SHT21_H
#define SHT21_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <inttypes.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

#define SHT21_ADDRESS 0x40
#define SHT21_TRIGGER_TEMP_MEASURE_NOHOLD  0xF3
#define SHT21_TRIGGER_HUMD_MEASURE_NOHOLD  0xF5

#endif
