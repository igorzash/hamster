#include "ccs811.h"

unsigned int ccs811_read_register(int fd, unsigned char register_no, int length, unsigned char *buffer)
{
  if (write(fd, &register_no, 1) != 1) {
    close(fd);
    perror("ccs811 read register: write error");
    return 1;
  }

  if (read(fd, buffer, length) != length) {
    close(fd);
    perror("ccs811 read register: read error");
    return 1;
  }

  return 0;
}

unsigned int ccs811_write_register1(int fd, unsigned char register_no, unsigned char data)
{
  unsigned char buffer[2];

  buffer[0] = register_no;
  buffer[1] = data;

  if (write(fd, buffer, 2) != 2) {
    close(fd);
    perror("ccs811 write_register1: write error");
    return 1;
  }

  return 0;
}

unsigned int ccs811_write_register0(int fd, unsigned char register_no)
{
  if (write(fd, &register_no, 1) != 1) {
    close(fd);
    perror("ccs811 write_register0: write error");
    return 1;
  }

  return 0;
}

int ccs811_setup(char* device)
{
  int fd = open(device, O_RDWR);

  ioctl(fd, I2C_SLAVE, CCS811_ADDRESS);

  return fd;
}

unsigned int ccs811_set_mode(unsigned int mode)
{
  int fd = ccs811_setup("/dev/i2c-1");
  return ccs811_write_register1(fd, CCS811_MEAS_MODE_REGISTER, mode);
}

struct ccs811_output ccs811_get()
{
  struct ccs811_output output;
  output.error = 1;

  int fd = ccs811_setup("/dev/i2c-1");
  unsigned char buffer[8];

  if(ccs811_read_register(fd, CCS811_STATUS_REGISTER, 1, buffer))
  {
    close(fd);
    return output;
  }

  unsigned int status = buffer[0];

  while (!(status & 8))
  {
    sleep(1);
    if(ccs811_read_register(fd, CCS811_STATUS_REGISTER, 1, buffer))
    {
      return output;
    }
    status = buffer[0];
  }

  if (ccs811_read_register(fd, CCS811_ALG_RESULT_DATA_REGISTER, 8, buffer))
  {
    return output;
  }

  output.error = 0;
  output.eco2 = (((unsigned int)buffer[0]) << 8) | buffer[1];
  output.tvoc = (((unsigned int)buffer[2]) << 8) | buffer[3];

  return output;
}

int ccs811_start()
{
  int fd = ccs811_setup("/dev/i2c-1");
  return ccs811_write_register0(fd, CCS811_APP_START_COMMAND);
}