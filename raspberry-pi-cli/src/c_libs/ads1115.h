#ifndef ADS115_H
#define ADS115_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <inttypes.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

#define ADS1115_ADDRESS 0x48

#endif // ADS1115_H
