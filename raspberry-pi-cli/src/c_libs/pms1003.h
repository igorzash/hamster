#ifndef PMS1003_H
#define PMS1003_H

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>

struct pms1003_output {
  unsigned int error;
  unsigned int particles[12];
};

#endif // PMS1003_H
