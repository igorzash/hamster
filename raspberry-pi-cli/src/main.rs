extern crate clap;
extern crate libc;

mod sensor_bindings;

use std::fmt::Display;

use sensor_bindings::{
    get_co2, get_dust_level, get_eco2_tvoc, get_gas_concentration, get_humidity, get_luminance,
    get_particles, get_temperature, set_ccs811_mode, start_ccs811,
};

fn print_option<T: Display>(opt: Option<T>) {
    match opt {
        None => println!("error"),
        Some(o) => println!("{}", o),
    }
}

fn main() {
    let matches = clap::App::new("raspberry")
        .version("beta")
        .subcommand(clap::SubCommand::with_name("co2"))
        .subcommand(clap::SubCommand::with_name("temperature"))
        .subcommand(clap::SubCommand::with_name("humidity"))
        .subcommand(clap::SubCommand::with_name("luminance"))
        .subcommand(clap::SubCommand::with_name("dust"))
        .subcommand(clap::SubCommand::with_name("gas"))
        .subcommand(clap::SubCommand::with_name("ccs811").subcommands(
            vec![clap::SubCommand::with_name("mode")
                    .arg(clap::Arg::with_name("mode").required(true).index(1)),
                    clap::SubCommand::with_name("start")],
        ))
        .subcommand(clap::SubCommand::with_name("eco2_tvoc"))
        .subcommand(clap::SubCommand::with_name("particles"))
        .get_matches();

    match matches.subcommand_name() {
        None => println!("No subcommand was given!"),
        Some("co2") => print_option(get_co2()),
        Some("temperature") => print_option(get_temperature()),
        Some("humidity") => print_option(get_humidity()),
        Some("luminance") => print_option(get_luminance()),
        Some("dust") => print_option(get_dust_level()),
        Some("gas") => print_option(get_gas_concentration()),
        Some("ccs811") => {
            let submatches = matches.subcommand_matches("ccs811").unwrap();

            match submatches.subcommand_name() {
                Some("mode") => {
                    let mode = submatches
                        .subcommand_matches("mode")
                        .unwrap()
                        .value_of("mode")
                        .unwrap()
                        .parse::<u32>()
                        .unwrap();

                    if !vec![0x0, 0x10, 0x20, 0x30, 0x40].contains(&mode) {
                        println!("Invalid mode");
                        return;
                    }

                    set_ccs811_mode(mode);

                    println!("[ccs811] Mode {} is set", mode);
                }
                Some("start") => start_ccs811(),
                _ => println!("No action given"),
            }
        }
        Some("eco2_tvoc") => {
            let output = get_eco2_tvoc();

            if output.error != 0 {
                println!("Error {}", output.error);
                return;
            }

            println!("{} {}", output.eco2, output.tvoc);
        },
        Some("particles") => {
            let output = get_particles();

            if output.error != 0 {
                println!("Error {}", output.error);
                return;
            }

            println!(
                "{}",
                &(output
                    .particles
                    .iter()
                    .fold("".to_string(), |x, y| x + " " + &y.to_string()))[1..]
            );
        }
        _ => {}
    }
}
