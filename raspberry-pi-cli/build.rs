use std::process::Command;
use std::fs::{create_dir_all};

fn main() {
    create_dir_all("obj").unwrap();
    create_dir_all("lib").unwrap();

    let mut c_lib_targets = vec!["ads1115",
                                 "ccs811",
                                 "max44009",
                                 "mh_z19b",
                                 "pms1003",
                                 "sht21"];

    while let Some(target) = c_lib_targets.pop() {
        let source_path = format!("src/c_libs/{}.c", target);
        let obj_path = format!("obj/{}.o", target);
        let lib_path = format!("lib/lib{}.a", target);

        Command::new("../cross-pi-gcc/bin/arm-linux-gnueabihf-gcc")
            .arg("-fPIC")
            .arg("-c")
            .arg(&source_path)
            .arg("-o")
            .arg(&obj_path)
            .status()
            .unwrap();

       Command::new("ar")
            .arg("rcs")
            .arg(&lib_path)
            .arg(&obj_path)
            .status()
            .unwrap();
    }

    println!("cargo:rustc-link-search=lib");
}
