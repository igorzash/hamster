import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SmallLiveValueDisplay extends StatelessWidget {
  final String title;
  final String valueId;
  final String unit;

  const SmallLiveValueDisplay({Key key, this.title, this.valueId, this.unit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Theme.of(context).accentColor,
      child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: StreamBuilder(
            stream: Firestore.instance
                .collection('sensor-data')
                .document(valueId)
                .snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Text('Loading...');
              }

              var value = snapshot.data['latest'];

              String valueStr = '';

              if (value is double) {
                valueStr = value.toStringAsFixed(1);
              } else {
                valueStr = value.toString();
              }

              if (unit != null) {
                valueStr += unit;
              }

              return Column(
                children: <Widget>[
                  Text(
                    title,
                    style: Theme.of(context).textTheme.caption,
                  ),
                  Text(
                    valueStr,
                    style: Theme.of(context).textTheme.body2,
                  ),
                ],
              );
            },
          )),
    );
  }
}
