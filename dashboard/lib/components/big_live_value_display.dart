import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dashboard/components/changelog_chart.dart';
import 'package:flutter/material.dart';

class BigLiveValueDisplay extends StatelessWidget {
  final Widget title;
  final String valueId;
  final String unit;

  const BigLiveValueDisplay(
      {Key key, @required this.title, @required this.valueId, this.unit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.black,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                  height: 200,
                  child: ChangelogChart(
                    valueId: valueId,
                  )),
            ),
            Column(
              children: <Widget>[
                title,
                StreamBuilder(
                  stream: Firestore.instance
                      .collection('sensor-data')
                      .document(valueId)
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Text('Loading...');
                    }

                    return Text.rich(
                      TextSpan(children: [
                        TextSpan(
                          text: snapshot.data['latest'].toString(),
                          style: Theme.of(context).textTheme.display1,
                        ),
                        TextSpan(
                            text: unit,
                            style: Theme.of(context).textTheme.caption)
                      ]),
                    );
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
