import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class ChangelogChart extends StatelessWidget {
  final String valueId;

  const ChangelogChart({Key key, @required this.valueId}) : super(key: key);

  @override
  Widget build(BuildContext context) => StreamBuilder(
        stream: Firestore.instance
            .collection('sensor-data')
            .document(valueId)
            .collection('changelog')
            .orderBy('timestamp', descending: true)
            .limit(60)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Text('Loading...');
          }

          return charts.TimeSeriesChart(
            [
              charts.Series(
                  id: valueId,
                  data: snapshot.data.documents,
                  colorFn: (_, __) =>
                      charts.MaterialPalette.indigo.shadeDefault,
                  domainFn: (doc, _) => doc['timestamp'].toDate(),
                  measureFn: (doc, _) => doc['value'])
            ]
          );
        },
      );
}
