import 'package:dashboard/components/big_live_value_display.dart';
import 'package:flutter/material.dart';

class MiddleContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        BigLiveValueDisplay(
            title: Text.rich(TextSpan(children: [
              TextSpan(text: 'CO'),
              TextSpan(
                  text: '2',
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: Colors.white))
            ])),
            valueId: 'co2',
            unit: ' ppm'),
        BigLiveValueDisplay(
            title: Text.rich(TextSpan(children: [
              TextSpan(
                  text: 'e',
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: Colors.white)),
              TextSpan(text: 'CO'),
              TextSpan(
                  text: '2',
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: Colors.white))
            ])),
            valueId: 'eco2',
            unit: ' ppm'),
        BigLiveValueDisplay(
            title: Text('TVOC'),
            valueId: 'tvoc',
            unit: ' ppb')
      ],
    );
  }
}
