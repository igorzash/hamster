import 'package:dashboard/components/small_live_value_display.dart';
import 'package:flutter/material.dart';

class TopContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(0.0),
        child: ListView.builder(
          shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return SmallLiveValueDisplay(
                title: ['Temperature', 'Humidity', 'Luminance'][index],
                valueId: ['temperature', 'humidity', 'luminance'][index],
                unit: ['° C', '%', ' lux'][index],
              );
            },
            itemCount: 3));
  }
}
