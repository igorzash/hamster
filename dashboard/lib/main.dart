import 'package:dashboard/components/middle_content.dart';
import 'package:flutter/material.dart';

import 'components/top_content.dart';

void main() => runApp(MaterialApp(
      theme: ThemeData(
        brightness: Brightness.dark,
        fontFamily: 'Montserrat',
        primaryColor: Colors.black,
        accentColor: Colors.indigo
      ),
      home: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Center(child: Text('Hamster Dashboard')),
        ),
        body: Column(
          children: <Widget>[
            Container(height: 50, child: TopContent()),
            Expanded(
              child: MiddleContent(),
            )
          ],
        ),
      ),
    ));
