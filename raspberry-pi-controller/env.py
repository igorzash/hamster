"""
env.py

Utils for reading environment variables.

"""

from os import environ


def process_environment_variable(item):
    """Returns environment value based on item type."""

    # Simply return value if str.
    # Warning: may cause IndexError.
    if isinstance(item, str):
        return environ[item]

    # Else expect item to be dict.
    if not isinstance(item, dict):
        raise ValueError(f'Expected str or dict, found {type(item)}.')

    # Type of the environment variable (default is str).
    env_type = item.get('type', str)

    # If default variable value is specified use get method.
    if 'default' in item.keys():
        return env_type(environ.get(item['name'], item['default']))

    # Otherwise expect environment variable to exist.
    # Warning: may cause IndexError.
    return env_type(environ[item['name']])


def read_environment_variables(names):
    return map(process_environment_variable,
               names)
