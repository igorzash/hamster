"""
cloud_mqtt.py

Provides abstractions for working with Cloud Cloud IoT Core MQTT Bridge.

"""

import logging
from datetime import datetime, timedelta
from jwt import encode as jwt_encode
from paho.mqtt.client import Client, error_string
from ssl import PROTOCOL_TLSv1_2


def create_jwt(project_id, private_key_file, algorithm):
    token = {
        # The time that the token was issued at
        'iat': datetime.utcnow(),
        # The time the token expires.
        'exp': datetime.utcnow() + timedelta(minutes=10),
        # The audience field should always be set to the GCP project id.
        'aud': project_id
    }

    # Read the private key file.
    with open(private_key_file, 'r') as f:
        private_key = f.read()

    logging.info('Creating jwt')

    return jwt_encode(token, private_key, algorithm=algorithm)


def error_str(rc):
    """Convert a Paho error to a human readable string."""
    return '{}: {}'.format(rc, error_string(rc))


def get_client(
        project_id, cloud_region, registry_id, device_id, private_key_file,
        algorithm, ca_certs, mqtt_bridge_hostname, mqtt_bridge_port,
        on_connect, on_publish, on_disconnect, on_message):
    """Create our MQTT client. The client_id is a unique string that identifies
    this device. For Google Cloud IoT Core, it must be in the format below."""
    client_id = 'projects/{}/locations/{}/registries/{}/devices/{}'.format(
        project_id, cloud_region, registry_id, device_id)
    logging.info('Device client_id is \'{}\''.format(client_id))

    client = Client(client_id=client_id)

    # With Google Cloud IoT Core, the username field is ignored, and the
    # password field is used to transmit a JWT to authorize the device.
    client.username_pw_set(
        username='unused',
        password=create_jwt(
            project_id, private_key_file, algorithm))

    client.tls_set(ca_certs=ca_certs, tls_version=PROTOCOL_TLSv1_2)

    # Set Paho callbacks
    client.on_connect = on_connect
    client.on_publish = on_publish
    client.on_disconnect = on_disconnect
    client.on_message = on_message

    client.connect(mqtt_bridge_hostname, mqtt_bridge_port)

    # This is the topic that the device will receive configuration updates on.
    mqtt_config_topic = '/devices/{}/config'.format(device_id)

    # Subscribe to the config topic.
    client.subscribe(mqtt_config_topic, qos=1)

    # The topic that the device will receive commands on.
    mqtt_command_topic = '/devices/{}/commands/#'.format(device_id)

    # Subscribe to the commands topic, QoS 1 enables message acknowledgement.
    logging.info('Subscribing to {}'.format(mqtt_command_topic))
    client.subscribe(mqtt_command_topic, qos=1)

    client.loop_start()

    return client
