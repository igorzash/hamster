"""
measurement_loop.py

Measurement processing loop. Defines ways of data collection.

"""

from queue import Queue
from time import sleep
from subprocess import run
from measurement import Measurement
from datetime import datetime

SLEEP_SECONDS = 10

CLI_AVAILABLE_MEASUREMENT_TYPES = (
    ('co2', int),
    ('temperature', float),
    ('humidity', float),
    ('dust', int),
    ('gas', int),
    ('luminance', float),
    ('eco2', int),
    ('tvoc', int),
    ('pm1.0-sp', int),
    ('pm2.5-sp', int),
    ('pm10-sp', int),
    ('pm1.0-uae', int),
    ('pm2.5-uae', int),
    ('pm10-uae', int),
    ('pb0.3', int),
    ('pb0.5', int),
    ('pb1', int),
    ('pb2.5', int),
    ('pb5', int),
    ('pb10', int)
)

data_queue = Queue()


def measurement_loop():
    while True:
        # Wait for new measurements if queue is fully processed.
        if data_queue.qsize() == 0:
            sleep(SLEEP_SECONDS)
            continue

        yield data_queue.get()


def pull_data_from_cli(cli_path):
    # simple refers to types, which have specific command.
    simple = CLI_AVAILABLE_MEASUREMENT_TYPES[:6]

    # Command returns two measurements at once.
    eco2_tvoc = CLI_AVAILABLE_MEASUREMENT_TYPES[6:8]

    # Many measurements at once.
    particles = CLI_AVAILABLE_MEASUREMENT_TYPES[8:]

    for (measurement_type, measurement_datatype) in simple:
        try:
            result = run([cli_path, measurement_type], capture_output=True)

            # Let the time when command execution ends be the timestamp
            timestamp = datetime.utcnow()

            measurement = Measurement(measurement_type,
                                      timestamp,
                                      measurement_datatype(
                                          str(result.stdout, 'UTF-8')))

            yield measurement
        except Exception as e:
            print(e)

    for (i, measurement_group) in enumerate((eco2_tvoc, particles)):
        try:
            result = run([cli_path, ('eco2_tvoc', 'particles')[i]],
                         capture_output=True)

            # Let the time when command execution ends be the timestamp
            timestamp = datetime.utcnow()

            raw_data = str(result.stdout, 'UTF-8').split()

            for (i, (measurement_type, measurement_datatype)) in\
                    enumerate(measurement_group):
                measurement = Measurement(measurement_type,
                                          timestamp,
                                          measurement_datatype(raw_data[i]))

                yield measurement
        except Exception as e:
            print(e)

    yield from []


def pull_data_from_cli_regular_job(cli_path):
    while True:
        for measurement in pull_data_from_cli(cli_path):
            data_queue.put(measurement)

        sleep(60)
