"""
measurement.py

Defines a way of representing measurement.

"""

from dataclasses import dataclass
from datetime import datetime
from typing import Any


@dataclass
class Measurement:
    type: str
    timestamp: datetime
    value: Any

    def to_payload(self) -> str:
        return ','.join((self.timestamp.strftime('%s'),
                         self.type,
                         str(self.value)))
