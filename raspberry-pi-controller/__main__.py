"""
__main__.py

Module entrypoint. Starts measuring and publishing data to Google Cloud
IoT Core.

"""

import logging
from datetime import datetime
from random import randint
from threading import Thread
from time import sleep

from cloud_mqtt import error_str, get_client
from env import read_environment_variables
from measurement_loop import measurement_loop, pull_data_from_cli_regular_job

if __name__ != '__main__':
    exit()

logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.CRITICAL)

logging.basicConfig(level=logging.INFO,
                    format='[%(asctime)s] %(levelname)s '
                    '(%(module)s) %(message)s')

environment = list(read_environment_variables(
    ('CLI_PATH',
     'PROJECT_ID',
     'CLOUD_REGION',
     'REGISTRY_ID',
     'DEVICE_ID',
     'PRIVATE_KEY_FILE',
     'ENCRYPTION_ALGHORITHM',
     'CA_CERTS',
     {'name': 'MQTT_BRIDGE_HOSTNAME', 'default': 'mqtt.googleapis.com'},
     {'name': 'MQTT_BRIDGE_PORT', 'type': int, 'default': 8883})))


cli_path,\
    project_id,\
    cloud_region,\
    registry_id,\
    device_id,\
    private_key_file,\
    encryption_alghorithm,\
    ca_certs,\
    mqtt_bridge_hostname,\
    mqtt_bridge_port = environment

cloud_environment = environment[1:]  # cli_path is unneeded

Thread(target=pull_data_from_cli_regular_job, args=(cli_path,)).start()

should_backoff = False
minimum_backoff_time = 1
MAXIMUM_BACKOFF_TIME = 32


def on_connect(unused_client, unused_userdata, unused_flags, rc):
    """Callback for when a device connects."""

    logging.info('Connected')

    # After a successful connect, reset backoff time and stop backing off.
    global should_backoff
    global minimum_backoff_time
    should_backoff = False
    minimum_backoff_time = 1


def on_disconnect(unused_client, unused_userdata, rc):
    """Paho callback for when a device disconnects."""

    logging.info('Disconnected ' + error_str(rc))

    # Since a disconnect occurred, the next loop iteration will wait with
    # exponential backoff.
    global should_backoff
    should_backoff = True


def on_publish(unused_client, unused_userdata, unused_mid):
    """Paho callback when a message is sent to the broker."""
    logging.info('Published')
    pass


def on_message(unused_client, unused_userdata, message):
    """Callback when the device receives a message on a subscription."""
    logging.info('Received message ' + message.topic)
    pass


def new_client():
    client = get_client(*(cloud_environment +
                          [on_connect,
                           on_publish,
                           on_message,
                           on_disconnect]))

    client.enable_logger(logging)

    return client


mqtt_topic = f'/devices/{device_id}/events'

jwt_iat = datetime.utcnow()  # issued at
jwt_exp_mins = 10  # expires in X minutes


client = new_client()

for measurement in measurement_loop():
    # Wait if backoff is required.
    if should_backoff:
        # If backoff time is too large, give up.
        if minimum_backoff_time > MAXIMUM_BACKOFF_TIME:
            logging.critical('Exceeded maximum backoff time. Giving up.')
            exit()

        # Otherwise, wait and connect again.
        delay = minimum_backoff_time + randint(0, 1000) / 1000.0
        sleep(delay)
        minimum_backoff_time *= 2
        client.connect(mqtt_bridge_hostname, mqtt_bridge_port)

    # Update jwt, in case if it expires
    seconds_since_issue = (datetime.utcnow() - jwt_iat).seconds
    if seconds_since_issue > 60 * jwt_exp_mins:
        client.disconnect()

        sleep(10)

        jwt_iat = datetime.utcnow()
        client = new_client()

    # Publish measurement
    client.publish(mqtt_topic, measurement.to_payload(), qos=1)

    sleep(1)
