import logging
from datetime import datetime, timezone
from os import environ

import apache_beam as beam
from apache_beam import Pipeline
from apache_beam.options.pipeline_options import (GoogleCloudOptions,
                                                  PipelineOptions,
                                                  StandardOptions)
from google.cloud import firestore

logging.basicConfig(level=logging.INFO,
                    format='[%(asctime)s] %(levelname)s '
                    '(%(module)s) %(message)s')

db = firestore.Client()

options = PipelineOptions()

google_cloud_options = options.view_as(GoogleCloudOptions)
google_cloud_options.project = environ['PROJECT']
google_cloud_options.job_name = environ["JOB"]

standard_options = options.view_as(StandardOptions)
standard_options.runner = environ['RUNNER']
standard_options.streaming = True

p = beam.Pipeline(options=options)


class ParsePubSubMessage(beam.DoFn):
    def process(self, msg):
        splitted = str(msg, 'utf-8').split(',')
        splitted[0] = datetime.utcfromtimestamp(int(splitted[0]))

        if '.' in splitted[-1]:
            splitted[-1] = float(splitted[-1])
        else:
            splitted[-1] = int(splitted[-1])

        item = dict()

        item['timestamp'], item['type'], item['value'] = splitted

        logging.info('Parsed ' + str(item))

        return [item]


class FilterInvalidData(beam.DoFn):
    def process(self, item):
        if item['type'] == 'eco2':
            if not (400 <= item['value'] <= 8192):
                return []
        
        if item['type'] == 'tvoc':
            if not (0 <= item['value'] <= 1187):
                return []
        
        return [item]


class WriteToFirestore(beam.DoFn):
    def process(self, item):
        type_document_ref = db.collection('sensor-data')\
            .document(item['type'])

        type_document_ref.collection('changelog')\
            .document()\
            .set({
                'timestamp': item['timestamp'],
                'value': item['value']
            })
        
        type_document_ref.set({
            'latest': item['value']
        })


p | beam.io.ReadFromPubSub(topic=environ['TOPIC'])\
  | beam.ParDo(ParsePubSubMessage())\
  | beam.ParDo(FilterInvalidData())\
  | beam.ParDo(WriteToFirestore())

result = p.run()
result.wait_until_finish()
